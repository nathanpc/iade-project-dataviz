/**
 * Restaurant.js
 * Restaurant image abstraction class.
 *
 * @author Nathan Campos <hi@nathancampos.me>
 */

class Restaurant {
	/**
	 * Class constructor.
	 *
	 * @param x       Center X position.
	 * @param y       Center Y position.
	 * @param name    Restaurant name.
	 * @param img_loc Image name.
	 */
	constructor(x, y, w, h, name, img_loc) {
		this.x = x;
		this.y = y;
		this.w = w;
		this.h = h;
		this.name = name;
		this.img = new ImageObject("img/logos/" + img_loc,
			this.x, this.y, this.w, this.h);
	}

	/**
	 * Draws the object.
	 */
	draw() {
		// Draw circle.
		noStroke();
		//fill(this.color);
		//circle(this.x, this.y, this.d);

		// Draw objects inside of it.
		this.img.draw();
	}
}
