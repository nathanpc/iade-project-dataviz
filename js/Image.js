/**
 * Image.js
 * Abstraction layer to show an image.
 *
 * @author Nathan Campos <hi@nathancampos.me>
 */

class ImageObject {
	/**
	 * Class constructor.
	 *
	 * @param image_loc Image file location.
	 * @param x         Center X position.
	 * @param y         Center Y position.
	 * @param w         Square width.
	 */
	constructor(image_loc, x, y, w = 150) {
		this.image_loc = image_loc;
		this.x = x;
		this.y = y;
		this.w = w;
		this.alpha = 255;

		if (image_loc != null) {
			this.img = loadImage(image_loc, function () {
				// Restart the draw routine to draw this image.
				start();
			}, function () {
				console.error("Error while loading image: '" + image_loc + "'");
			});
		}
	}

	/**
	 * Sets the transparency of the whole object.
	 *
	 * @param alpha Amount of opacity.
	 */
	setAlpha(alpha) {
		this.alpha = alpha;
	}

	/**
	 * Draws the food image.
	 */
	draw() {
		// Use a rectangle placeholder.
		if (this.image_loc == null) {
			rect(this.x - this.w / 2,
				 this.y - this.w / 2,
				 this.w,
				 this.w);

			return;
		}

		if (this.alpha < 255) {
			tint(255, this.alpha);
		} else {
			noTint();
		}

		image(this.img,
			  this.x - (this.w / 2),
			  this.y - (this.w / 2),
			  this.w, this.w, 0, 0);
		noTint();
	}

	/**
	 * Gets the top position of the object.
	 *
	 * @return Object with top X and Y.
	 */
	top() {
		return {
			x: this.x,
			y: this.y - this.w / 2
		};
	}

	/**
	 * Gets the bottom position of the object.
	 *
	 * @return Object with bottom X and Y.
	 */
	bottom() {
		return {
			x: this.x,
			y: this.y + this.w / 2
		};
	}

	/**
	 * Checks if a mouse is hovering the item.
	 *
	 * @return True if the mouse is hovering the item.
	 */
	mouseHovering() {
		return (mouseY >= this.top().y) &&
			(mouseY <= this.bottom().y) &&
			(mouseX >= (this.x - this.w / 2)) &&
			(mouseX <= (this.x + this.w / 2));
	}
}
