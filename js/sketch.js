/**
 * sketch.js
 * Main project p5 sketch.
 *
 * @author Nathan Campos <hi@nathancampos.me>
 */

var data = null;
var title;
var meals = [];
var restaurants = [];
var selected_index = -1;
var title_img;
var back_img;
var help_img;
var last_pos_selected = {
	x: null,
	y: null
};

/**
 * Load everything before p5 creates the canvas.
 */
function preload() {
	// Load title image.
	title_img = loadImage("img/logos/project.png");
	back_img = new ImageObject("img/buttons/back.png", 70, windowHeight - 70, 70);
	help_img = new ImageObject("img/buttons/help.png", windowWidth - 70, windowHeight - 70, 70);

	// Grab the data and populate the screen.
	getData();
}

/**
 * Setup p5.
 */
function setup() {
	// Create canvas.
	var canvas = createCanvas(windowWidth, windowHeight);
	canvas.parent("canvas-container");

	// Set title.
	title = new Label("Don't be Fooled!", 300, 20, 50, BOLD);

	// Call resize just to adjust things.
	//windowResized();

	console.log("Center: (" + width / 2 + ", " + height / 2 + ")");
}

/**
 * p5 draw loop.
 */
function draw() {
	// Set background to clear the canvas.
	background(220);

	// Check if the data is present for us to show the items.
	if (data != null) {
		// Show a single food.
		if (showSingle(selected_index)) {
			show_title_logo(250, 100);

			// Show back button.
			if (back_img.mouseHovering()) {
				back_img.setAlpha(255);
				cursor(HAND);
			} else {
				back_img.setAlpha(210);
				cursor(ARROW);
			}
			back_img.draw();

			stop();
			return;
		}

		// Show help button.
		help_img.setAlpha(210);
		if (help_img.mouseHovering()) {
			help_img.setAlpha(255);

			// Show help text.
			push();
			textSize(16);
			textAlign(RIGHT);
			fill(50);
			stroke(50);
			text("Using this application is very simple! Just click on the meal\nyou want to know more about and you'll get all the information\nyou'll need in the next screen. To go back just hit the back\nbutton in the bottom left corner.", width - (70 * 2), height - (70 / 2));
			pop();
		}
		help_img.draw();

		// Show logo.
		show_title_logo(width / 2, 100);

		// Check for hovering.
		var hovered_item = -1;
		for (var i = 0; i < meals.length; i++) {
			if (meals[i].mouseHovering()) {
				hovered_item = i;
			}
		}

		// Set the cursor to a hand when hovering a item.
		if (hovered_item >= 0) {
			cursor(HAND);
		} else {
			cursor(ARROW);
		}

		// Draw restaurant logos.
		for (var i = 0; i < restaurants.length; i++) {
			restaurants[i].draw();
		}

		// Draw food objects.
		for (var i = 0; i < meals.length; i++) {
			// Check for hovering items.
			if (hovered_item >= 0) {
				if (i != hovered_item) {
					meals[i].setAlpha(100);
				} else {
					meals[i].setAlpha(255);
				}
			} else {
				meals[i].setAlpha(255);
			}

			meals[i].draw();
		}
	} else {
		// Show a little loading thingy.
		textAlign(CENTER, CENTER);
		textSize(30);
		stroke(50);
		fill(50);

		// Draw text.
		text("Loading...", width / 2, 300);
	}

	// Conserve the CPU.
	stop();
}

/**
 * Mouse clicked event handler.
 */
function mouseClicked() {
	if ((selected_index >= 0) && (data != null)) {
		// Return to the main menu.
		if (back_img.mouseHovering()) {
			backToGrid();
			selected_index = -1;
			title.x = width / 2;
			title.set_text("Don't be Fooled!");
		}
	} else {
		// Check if an item was clicked and select it.
		for (var i = 0; i < meals.length; i++) {
			if (meals[i].mouseHovering()) {
				selected_index = i;
				last_pos_selected.x = meals[i].x;
				last_pos_selected.y = meals[i].y;
				meals[i].kcal = 0;

				title.x = 300;
				title.set_text("Don't be Fooled by a\n" + meals[i].name + "!");
			}
		}
	}

	// Restart the draw loop.
	start();
}

/**
 * Draws our project logo.
 *
 * @param x Center X position.
 * @param y Center Y position.
 * @param w Image width.
 * @param h Image height.
 */
function show_title_logo(x, y, w = 400, h = 200) {
	image(title_img, x - (w / 2), y - (h / 2), w, h, 0, 0);
}

/**
 * Shows a single meal.
 *
 * @param index Meal index.
 */
function showSingle(index) {
	if (index >= 0) {
		meals[index].restaurant.draw();
		meals[index].moveTo(width / 2, height / 2, true);
		//meals[index].translateTo(width / 2, height / 2, 1);
		meals[index].drawExercises();
		meals[index].setScale(2.0, true);
		meals[index].draw();
		scale(0.5);

		return true;
	}

	return false;
}

/**
 * Goes back to the grid.
 */
function backToGrid() {
	scale(1);
	meals[selected_index].setScale(1, false);
	meals[selected_index].moveTo(last_pos_selected.x, last_pos_selected.y, false);
}

/**
 * Restarts the draw loop.
 */
function start() {
	loop();
	//console.log("Restarted the drawing loop.");
}

/**
 * Stops the draw loop.
 * *DEACTIVATED*
 */
function stop() {
	//noLoop();
	//console.log("Drawing loop stopped.");
}

/**
 * Window resized event handler.
 */
function windowResized() {
	// Resize the canvas.
	resizeCanvas(windowWidth, windowHeight);

	back_img = new ImageObject("img/buttons/back.png", 70, windowHeight - 70, 70);
	help_img = new ImageObject("img/buttons/help.png", windowWidth - 70, windowHeight - 70, 70);

	if (data != null) {
		title.x = width / 2;

		// Populate the meals array and layout the grid.
		populateGrid();
	}
}

/**
 * Grabs the data and populates our global variables.
 */
function getData() {
	var req = new XMLHttpRequest();
	req.addEventListener("load", function () {
		// Store the received data in our global variable.
		data = JSON.parse(this.responseText);
		console.log(data);

		// Populate the meals array and layout the grid.
		populateGrid();

		start();
	});

	req.open("GET", "data.json");
	req.send();
}

/**
 * Populates the meals array and arranges them in the grid.
 *
 * @param food_width  Food object full width.
 * @param food_height Food object full height.
 * @param padding     Minimum padding between each food object.
 */
function populateGrid(food_width = 250, food_height = 300, padding = 50) {
	// Clear the arrays.
	meals.length = 0;
	restaurants.length = 0;

	// Loop through restaurants.
	for (var i = 0; i < data.restaurants.length; i++) {
		var rest = data.restaurants[i];
		var center_pos = {
			x: (((windowWidth - 100) / data.restaurants.length) * i) + food_width + 100,
			y: food_height + 70
		};

		if (i == 1) {
			center_pos.y = windowHeight - food_height - 20;
		}

		restaurants.push(new Restaurant(center_pos.x, center_pos.y - 30, 100, 100,
										rest.name, rest.image));

		// Populate meals.
		var cur_idx = 0;
		for (var j = 0; j < data.food.length; j++) {
			// Check if this is inside the restaurant.
			if (data.food[j].brand == rest.name) {
				// Generate its position in the grid.
				var pos = radialCoord(center_pos.x, center_pos.y, (360 / 4) * j,
									  food_height * 0.7);

				// Push meal to array.
				var food = data.food[j];
				meals.push(new Food(pos.x, pos.y, food_width, food.name,
									food.calories, food.image, data.exercises,
									new Restaurant(windowWidth - 160, 110, 200, 200,
												   rest.name, rest.image)));
			}
		}
	}
}

/**
 * Gets the coordinate from a point to another using a distance and an
 * angle. This is great to make radial layouts.
 *
 * @param  angle Angle of the line.
 * @param  dist  Distance from center.
 * @return       X and Y coordinate.
 */
function radialCoord(x, y, angle, dist) {
	return {
		x: x + (dist * cos((Math.PI / 180) * angle)),
		y: y + (dist * sin((Math.PI / 180) * angle))
	};
}
