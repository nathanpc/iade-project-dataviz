/**
 * ExerciseBubble.js
 * Exercise bubble abstraction class.
 *
 * @author Nathan Campos <hi@nathancampos.me>
 */

class ExerciseBubble {
	/**
	 * Class constructor.
	 *
	 * @param x         Center X position.
	 * @param y         Center Y position.
	 * @param time      Exercise time in minutes.
	 * @param image_loc Exercise icon image location.
	 * @param d         Circle diameter.
	 * @param color     Circle background color.
	 */
	constructor(x, y, time, image_loc, d = 150, color = 100) {
		this.x = x;
		this.y = y;
		this.d = d;
		this.color = color;

		// Time label.
		this.label = new Label(round(time) + " min", this.x, this.y + (d / 2) - 50, 20);
		this.label.color = 50;

		// Exercise image.
		this.img = new ImageObject("img/sports/" + image_loc, this.x, this.y - 15, (this.d / 2) - 10);
	}

	/**
	 * Draws the object.
	 */
	draw() {
		// Draw circle.
		noStroke();
		//fill(this.color);
		//circle(this.x, this.y, this.d);

		// Draw objects inside of it.
		this.label.draw();
		this.img.draw();
	}
}
