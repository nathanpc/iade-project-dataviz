/**
 * EnergyBar.js
 * A visual way a representing the energy in a meal.
 *
 * @author Nathan Campos <hi@nathancampos.me>
 */

class EnergyBar {
	/**
	 * Class constructor.
	 *
	 * @param x            Bottom center arc X position.
	 * @param y            Bottom center arc Y position.
	 * @param w            Object width.
	 * @param bar_h        Bar height.
	 * @param bg_color     Bar background color.
	 * @param fg_color     Bar foreground color.
	 * @param show_outline Show the outline around the bar.
	 */
	constructor(x, y, w = 250, bar_h = 30, bg_color = 240, fg_color = "green",
				show_outline = false) {
		this.x = x;
		this.y = y;
		this.w = w;
		this.bar_h = bar_h;
		this.bg_color = color(bg_color);
		this.fg_color = color(fg_color);
		this.alpha = 255;
		this.show_outline = show_outline;
		this.perc = 0;
	}

	/**
	 * Sets the transparency of the whole object.
	 *
	 * @param alpha Amount of opacity.
	 */
	setAlpha(alpha) {
		this.alpha = alpha;
		this.fg_color.setAlpha(alpha);
		this.bg_color.setAlpha(alpha);
	}

	/**
	 * Sets the percentage of the bar.
	 *
	 * @param perc New percentage value.
	 */
	set_percentage(perc) {
		// Simple bounds checking.
		if (perc > 100) {
			perc = 100;
		} else if (perc < 0) {
			perc = 0;
		}

		this.perc = perc;
	}

	/**
	 * Sets the foreground color of the bar.
	 *
	 * @param fg_color Bar color.
	 * @param alpha    Bar transparency.
	 */
	set_color(fg_color, alpha = this.alpha) {
		this.fg_color = color(fg_color);
		this.setAlpha(this.alpha);
	}

	/**
	 * Draws object.
	 */
	draw() {
		noFill();

		// Inside bar background.
		stroke(this.bg_color);
		strokeWeight(this.bar_h);
		arc(this.x,
			this.y,
			this.w - this.bar_h,
			this.w - this.bar_h,
			PI,
			TWO_PI,
			OPEN);

		// Inside bar foreground.
		if (this.perc > 0) {
			stroke(this.fg_color);
			arc(this.x,
				this.y,
				this.w - this.bar_h,
				this.w - this.bar_h,
				PI,
				map(this.perc, 0, 100, PI, TWO_PI),
				OPEN);
		}

		// Reset some attributes.
		stroke(50);
		strokeWeight(1);
		noFill();

		if (this.show_outline) {
			// Top portion.
			arc(this.x,
				this.y,
				this.w,
				this.w,
				PI,
				TWO_PI,
				OPEN);

			// Bottom portion.
			arc(this.x,
				this.y,
				this.w - this.bar_h * 2,
				this.w - this.bar_h * 2,
				PI,
				TWO_PI,
				OPEN);

			// Left close arc.
			arc(this.x - (this.w / 2) + (this.bar_h / 2),
				this.y,
				this.bar_h,
				this.bar_h,
				TWO_PI,
				PI,
				OPEN);

			// Right close arc.
			arc(this.x + (this.w / 2) - (this.bar_h / 2),
				this.y,
				this.bar_h,
				this.bar_h,
				TWO_PI,
				PI,
				OPEN);
		} else {
			noStroke();
		}
	}

	/**
	 * Gets the top position of the object.
	 *
	 * @return Object with X and Y position of the top of the bar.
	 */
	top() {
		return {
			x: this.x,
			y: this.y - this.w / 2
		};
	}
}
