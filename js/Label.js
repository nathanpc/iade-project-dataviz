/**
 * Label.js
 * A simple label object abstraction.
 *
 * @author Nathan Campos <hi@nathancampos.me>
 */

class Label {
	/**
	 * Label class contructor.
	 *
	 * @param text      Label text.
	 * @param x         Object X position.
	 * @param y         Object Y position.
	 * @param txt_size  Text size.
	 * @param txt_style Text style.
	 * @param x_align   How to align the label in the X position.
	 * @param y_align   How to align the label in the Y position.
	 * @param _color    Text color.
	 */
	constructor(text, x, y, txt_size = 16, txt_style = NORMAL, x_align = CENTER,
				y_align = TOP, _color = 30) {
		this.text = text;
		this.x = x;
		this.y = y;
		this.txt_size = txt_size;
		this.txt_style = txt_style;
		this.x_align = x_align;
		this.y_align = y_align;
		this.color = color(_color);
		this.alpha = 255;
	}

	/**
	 * Sets the transparency of the whole object.
	 *
	 * @param alpha Amount of opacity.
	 */
	setAlpha(alpha) {
		this.alpha = alpha;

		// Draw objects.
		this.color.setAlpha(alpha);
	}

	/**
	 * Sets the label color.
	 *
	 * @param _color Label color.
	 * @param alpha  Label transparency.
	 */
	set_color(_color, alpha = this.alpha) {
		this.color = color(_color);
		this.setAlpha(alpha);
	}

	/**
	 * Sets the label text.
	 *
	 * @param text Label text.
	 */
	set_text(text) {
		this.text = text;
	}

	/**
	 * Gets the top position of the object.
	 *
	 * @return Object with the top X and Y position of the object.
	 */
	top() {
		return {
			x: this.x,
			y: this.y - this.txt_size / 2
		};
	}

	/**
	 * Gets the bottom position of the object.
	 *
	 * @return Object with the bottom X and Y position of the object.
	 */
	bottom() {
		return {
			x: this.x,
			y: this.y + this.txt_size / 2
		};
	}

	/**
	 * Draws object.
	 */
	draw() {
		// Setup text properties.
		textAlign(this.x_align, this.y_align);
		textSize(this.txt_size);
		textStyle(this.txt_style);
		stroke(this.color);
		fill(this.color);

		// Draw text.
		text(this.text, this.x, this.y);
	}
}
