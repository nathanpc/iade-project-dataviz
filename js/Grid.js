/**
 * Grid.js
 * Little grid layout helper class.
 *
 * @author Nathan Campos <hi@nathancampos.me>
 */

class Grid {
	/**
	 * Class constructor.
	 *
	 * @param x          Top left corner X position.
	 * @param y          Top left corner Y position.
	 * @param win_width  Window width.
	 * @param obj_width  Object width.
	 * @param obj_height Object height.
	 * @param padding    Size padding width.
	 */
	constructor(x, y, win_width, obj_width, obj_height, padding) {
		this.x = x;
		this.y = y;
		this.win_width = win_width;
		this.obj_width = obj_width;
		this.obj_height = obj_height;
		this.padding = padding;

		this.max_cols = int((win_width - (padding * 2)) / (obj_width + padding));
		this.spacing = (win_width - (padding * 2) - (obj_width * this.max_cols))
			/ (this.max_cols - 1);
		if (this.max_cols == 1) {
			this.spacing = (win_width - (padding * 2) - (obj_width * this.max_cols));
		}
	}

	/**
	 * Get the number of columns per row.
	 *
	 * @return Columns per row.
	 */
	max_columns() {
		return this.max_cols;
	}

	/**
	 * Gets the X and Y position for an item by its index.
	 *
	 * @param  index Item index.
	 * @return       Object with X and Y position for that cell.
	 */
	position(index) {
		var row = int(index / this.max_columns());
		var col = index - (this.max_columns() * row);

		return this.cell_position(row, col);
	}

	/**
	 * Gets the X and Y position for an item by a row and column.
	 *
	 * @param  row Item row.
	 * @param  col Item column.
	 * @return     Object with X and Y position for that cell.
	 */
	cell_position(row, col) {
		return {
			x: this.x + this.padding + (this.obj_width / 2) + (col * (this.obj_width + this.spacing)),
			y: this.y + this.padding + (this.obj_height / 2) + (row * this.obj_height)
		};
	}
}
