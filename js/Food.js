/**
 * Food.js
 * Abstraction of a food item.
 *
 * @author Nathan Campos <hi@nathancampos.me>
 */

class Food {
	/**
	 * Food item class constructor.
	 *
	 * @param x         Center X position.
	 * @param y         Center Y position.
	 * @param name      Food name.
	 * @param kcal      Food energy.
	 * @param image     Food image.
	 * @param exercises Array of exercise data.
	 * @param rest      Restaurant object.
	 */
	constructor(x, y, w, name, kcal, image, exercises, rest) {
		this.x = x;
		this.y = y;
		this.w = w;
		this.name = name;
		this.kcal = 0;
		this.exercise_data = exercises;
		this.restaurant = rest;
		this.scale = 1.0;
		this.moveTheta = null;
		this.moveX = 0;
		this.moveY = 0;
		this.targetKcal = kcal;
		this.incrementSpeed = round(random(8, 20));
		this.alpha = 255;

		// Create objects.
		this.bar = new EnergyBar(0, 0, this.w);
		this.img = new ImageObject("img/food/" + image, 0, 0);
		this.lbl_name = new Label(this.name, 0, 0, 20, BOLD);
		this.lbl_cal = new Label("ND", 0, 0, 16, NORMAL, CENTER, BOTTOM);
		this.moveTo(this.x, this.y);

		// Set parameters.
		this.setEnergy(this.kcal);

		// Populates the exercises bubble.
		this.exercises = [];
		this.populateExercises();
	}

	/**
	 * Sets the transparency of the whole object.
	 *
	 * @param alpha Amount of opacity.
	 */
	setAlpha(alpha) {
		this.alpha = alpha;
		this.bar.setAlpha(alpha);
		this.img.setAlpha(alpha);
		this.lbl_name.setAlpha(alpha);
		this.lbl_cal.setAlpha(alpha);
	}

	/**
	 * Gets the coordinate from a point to another using a distance and an
	 * angle. This is great to make radial layouts.
	 *
	 * @param  angle Angle of the line.
	 * @param  dist  Distance from center.
	 * @return       X and Y coordinate.
	 */
	radialCoordinate(angle, dist) {
		return {
			x: (windowWidth / 2) + (dist * cos((Math.PI / 180) * angle)),
			y: (windowHeight / 2) + (dist * sin((Math.PI / 180) * angle))
		};
	}

	/**
	 * Populates the exercise array.
	 */
	populateExercises() {
		// Reset the array.
		this.exercises.length = 0;

		// Populate it with bubbles.
		for (var i = 0; i < this.exercise_data.length; i++) {
			var coord = this.radialCoordinate((360 / this.exercise_data.length) * i, 350);

			this.exercises.push(new ExerciseBubble(coord.x, coord.y,
												   this.targetKcal / this.exercise_data[i].calories,
												   this.exercise_data[i].image));
		}
	}

	/**
	 * Sets the amount of energy in a meal.
	 *
	 * @param kcal Meal energy.
	 */
	setEnergy(kcal) {
		// Set bar color according to energy amount.
		var color = "red";
		if (kcal <= 200) {
			color = "green";
		} else if (kcal <= 600) {
			color = "orange";
		}

		// Set the bar level and label.
		this.kcal = kcal;
		this.bar.set_color(color);
		this.lbl_cal.set_color(color);
		this.bar.set_percentage(map(this.kcal, 0, 2000, 0, 100));
		this.lbl_cal.set_text(this.kcal + " kcal");
	}

	/**
	 * Sets the amount of energy in a meal in a animated way.
	 *
	 * @param kcal      Meal energy.
	 * @param increment Animation increment amount.
	 */
	animateEnergy(kcal, increment) {
		if ((this.kcal < kcal) && ((this.kcal + increment) < kcal)) {
			this.setEnergy(this.kcal + increment);
		} else {
			this.setEnergy(kcal);
		}
	}

	/**
	 * Moves the whole object to a position.
	 *
	 * @param x          X position to move.
	 * @param y          Y position to move.
	 * @param cal_bottom Put the calories display on the bottom.
	 */
	moveTo(x, y, cal_bottom = false) {
		// Change this object's position.
		this.x = x;
		this.y = y;

		// Change the bar position.
		this.bar.x = this.x;
		this.bar.y = this.y;

		// Change the image position.
		this.img.x = this.x;
		this.img.y = this.y;

		// Change the label positions.
		this.lbl_name.x = this.img.bottom().x;
		this.lbl_name.y = this.img.bottom().y + 5;
		this.lbl_cal.x = this.bar.top().x;
		this.lbl_cal.y = this.bar.top().y - 10;

		// Put the calories in the bottom if needed.
		if (cal_bottom) {
			this.lbl_cal.y = this.lbl_name.bottom().y + 30;
		}
	}

	/**
	 * Sets a scale for the object. This function can be called inside the draw
	 * loop and should be called before this.draw() to take effect.
	 *
	 * @param s          Scale to be set.
	 * @param cal_bottom Put the calories display on the bottom.
	 */
	setScale(s, cal_bottom = false) {
		if (s != this.scale) {
			this.moveTo(this.x /= s, this.y /= s, cal_bottom);
			scale(s);
		}
	}

	/**
	 * Moves the whole object to a position while doing a transition.
	 *
	 * @param x        X position to move.
	 * @param y        Y position to move.
	 * @param speed    Moving speed.
	 * @param finished Callback for when the animation finishes.
	 */
	translateTo(x, y, speed, finished) {
		// Do nothing if we are already at the destination.
		if ((this.x == x) && (this.y == y)) {
			if (finished !== undefined) {
				finished();
			}

			return;
		}

		// Check if our target changed and calculate a new theta.
		if ((x != this.moveX) || (y != this.moveY)) {
			this.moveTheta = Math.atan2(x - this.x, y - this.y);
			if (this.moveTheta < 0.0)
				this.moveTheta += TWO_PI;

			console.log(this.x);
			console.log(this.y);
			console.log(x);
			console.log(y);
			console.log(this.moveTheta * (180 / Math.PI));
			console.log(this.moveTheta);
			this.moveX = x;
			this.moveY = y;
		}

		// Move the item to the next point.
		this.moveTo(this.x + (speed * Math.cos(this.moveTheta)),
					this.y + (speed * Math.sin(this.moveTheta)));
	}

	/**
	 * Draws the exercise bubbles.
	 */
	drawExercises() {
		for (var i = 0; i < this.exercises.length; i++) {
			this.exercises[i].draw();
		}
	}

	/**
	 * Draws the whole food object.
	 */
	draw() {
		// Animate the energy bar.
		this.animateEnergy(this.targetKcal, this.incrementSpeed);

		// Draw objects.
		this.bar.draw();
		this.img.draw();
		this.lbl_name.draw();
		this.lbl_cal.draw();

		// Center crosshair.
		/*stroke("red");
		line(this.x - 20, this.y, this.x + 20, this.y);
		line(this.x, this.y - 20, this.x, this.y + 20);*/
	}

	/**
	 * Checks if a mouse is hovering the item.
	 *
	 * @return True if the mouse is hovering the item.
	 */
	mouseHovering() {
		var top = this.lbl_cal.top().y;
		var left = this.bar.x - (this.bar.w / 2);
		var bottom = this.lbl_name.bottom().y;
		var radius = this.bar.w / 2;
		var middle = {
			x: left + (this.bar.w / 2),
			y: top + ((bottom - top) / 2)
		};

		return int(dist(middle.x, middle.y, mouseX, mouseY)) < radius;
	}
}
