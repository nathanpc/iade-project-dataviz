# Don't be Fooled!

This is a data visualization project about food consumption and exercising
developed for the first semester of the Creative Technologies course of IADE.

Don't judge the commits and my Git skills by this repo. I used Git here just to
keep a good backup of the project. For professional projects please take a look
at [**my GitHub page**](https://github.com/nathanpc).

## License

This project is licensed under the [GNU General Public License v3.0](https://www.gnu.org/licenses/gpl-3.0.html).

![Project Logo](https://bitbucket.org/nathanpc/iade-project-dataviz/raw/a0807ec93f3429dff5261060a97e03e07aa70227/img/logos/project.png)

